﻿using System;
using System.Collections.Generic;

namespace Task10Zoology
{
    public enum Gender
    {
        Female,
        Male
    }

    [Flags]
    public enum Skills
    {
        None = 1,
        Biter = 2,
        Climber = 4,
        Runner = 8,
    }

    public abstract class Animal
    {
        public string Name{ get; set; }
        public string Species { get; set; }
        public string Breed { get; set; }

        public double Weight { get; set; }
        public double Height { get; set; }

        public int Age { get; set; }
        public Gender Gender { get; set; }
        public Skills Skills { get; set; }

        public Animal(string name, string species, string breed, int age, double weight, double height)
        {
            Name = name;
            Species = species;
            Breed = breed;
            Age = age;
            Gender = Gender.Male;
            Skills = Skills.None;
            Weight = weight;
            Height = height;
        }

        public Animal(string name, string species, string breed, int age, double weight, double height, Gender gender = Gender.Male)
        {
            Name = name;
            Species = species;
            Breed = breed;
            Age = age;
            Gender = gender;
            Skills = Skills.None;
            Weight = weight;
            Height = height;
        }

        public virtual void Kill(Animal animal)
        {
            Utilities.Print($"{Name} found and killed a {animal.Species} named {animal.Name}. :(");
        }

        public virtual void Yawn()
        {
            Utilities.Print($"{Species}::{Name} yawned. Maybe a bit tired ayy?");
        }

        public virtual void PrintDescription()
        {
            Utilities.Print($"{Name} is a {Gender.ToString().ToLower()} {Species} who is {Age} years old and weighs {Weight}kg and is {Height}cm tall.");
        }

        public void PrintSkills()
        {
            Utilities.Print($"{Name} (A {Breed}) has the following skills: {Skills}");
        }
    }
}
