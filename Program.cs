﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task10Zoology
{
    class Program
    {
        static List<Animal> pets;

        #region Filter Mode Variables
        static AnimalFilter filterMode;
        static bool fFiltering; // Is the application in filter mode
        #endregion

        // The main function that starts everything up.
        static void Main(string[] args)
        {
            filterMode = new AnimalFilter();
            BigBang();
        }

        /*
         * Print out information about the program and list available options
         */
        static void Info()
        {
            Utilities.Print($"\n\nWelcome to the San Diego Zoo. This zoo currently houses {pets.Count} animals.");
            Utilities.Print("You are in charge of managing our zoo's animals.\nAvailable options: ");
            Utilities.Print(" - animals: List all animals living in the zoo");
            Utilities.Print(" - species: List all animal species");
            Utilities.Print(" - skills: List all animals by skill");
            Utilities.Print(" - filter: Filter through the animals");
            Utilities.Print(" - exit: Quit the applications");
        }

        /*
         * Create instances of all animals.
         */
        static void BigBang()
        {
            Cat stella = new Cat("Stella", 8, 4.0, 23, Gender.Female);
            Cat franky = new Cat("Franky", 4, 7.0, 26, Gender.Male);

            Dog whiskey = new Dog("Whiskey", "Dalmatian", 12, 12.0, 33, Gender.Male);
            Dog lucy = new Dog("Lucy", "Pinscher", 4, 7.0, 13, Gender.Female);
            Greyhound hugh = new Greyhound("Hugh", 10, 6.0, 20, Gender.Male);
            GermanShepherd bella = new GermanShepherd("Bella", 13, 34.0, 30, Gender.Female);

            Tiger vanessa = new Tiger("Vanessa", 6, 60, 70.3, Gender.Female, "Bengal");
            Lion grant = new Lion("Grant", 6, 7.0, 23, Gender.Male, "Southwest African");
            Liger jerry = new Liger("Jerry", 3, 5.0, 12, Gender.Male);
            Tigon anna = new Tigon("Anna", 7, 3.7, 56, Gender.Female);
            Jaguar coco = new Jaguar("Coco", 5, 4.2, 32, Gender.Female, "Panthera onca onca");

            /* Crossbreeding
            Cat edward = vanessa.Crossbreed(grant, "Edward");
            Cat brandy = grant.Crossbreed(vanessa, "Brandy");
            franky.Kill(whiskey);*/

            pets = new List<Animal>()
            {
                stella,
                franky,
                whiskey,
                lucy,
                vanessa,
                grant,
                jerry,
                hugh,
                anna,
                coco,
                bella,
                /*edward,
                brandy,*/
            };

            pets = (from pet in pets
                   orderby pet.Age ascending
                   select pet).ToList();

            bool run = true;
            while (run)
            {
                Info();
                run = UserInput();
            }
        }

        /*
         * Take care of user input and command handling
         */
        static bool UserInput()
        {
            bool run = true;
            Console.Write("San Diego Zoo CLI> ");
            string input = Console.ReadLine();

            switch(input)
            {
                case "animals":
                    ViewAllDetails();
                    break;
                case "species":
                    int speciesCount = 0;
                    Dictionary<string, int> species = new Dictionary<string, int>();

                    foreach (Animal animal in pets)
                    {
                        if (!species.ContainsKey(animal.Species))
                        {
                            species.Add(animal.Species, 1);
                            speciesCount++;
                        }
                        else
                            species[animal.Species]++;
                    }

                    IEnumerable<KeyValuePair<string, int>> orderedSpecies = from s in species
                                                         orderby s.Key ascending
                                                         select s;

                    Utilities.Print($"This zoo has a total of {speciesCount} species: ", ConsoleColor.Cyan);

                    foreach (KeyValuePair<string, int> aSpecies in orderedSpecies)
                        Utilities.Print($"  - {aSpecies.Value} {aSpecies.Key}{(aSpecies.Value > 1 ? "s" : "")}", ConsoleColor.DarkCyan);
                    break;
                case "skills":
                    IEnumerable<Animal> animalSkills = filterMode.Filter<Skills>(pets, AnimalFilter.Filters.BySkill, (pet, val) => {
                        return pet.Skills != val;
                    }, Skills.None);

                    foreach (Animal animal in animalSkills)
                        animal.PrintSkills();
                    break;
                case "filter":
                    filterMode.FilterSearch(pets);
                    break;
                case "exit":
                    run = false;
                    break;
            }

            return run;
        }

        /*
         * List all details about all animals, and show off some of the methods,
         * such as murder...
         */
        static void ViewAllDetails()
        {
            Utilities.Print($"\nPrinting out animal descriptions", ConsoleColor.DarkYellow);

            Animal lastAnimal = null;
            foreach (Animal pet in pets)
            {
                pet.PrintDescription();
                pet.Yawn();

                if (lastAnimal != null)
                    lastAnimal.Kill(pet);

                lastAnimal = pet;
            }

            ListRunners();
            ListClimbers();
            ListBiters();
        }

        /*
         * The methods below will retrieve each animal which inherit IRunner, IClimber or IBiter
         * and then output the responses that each animal has for each call to the interfaced
         * methods.
         */

        static void ListRunners()
        {
            Utilities.Print("\nListing all animals capable of running: ", ConsoleColor.Yellow);

            IEnumerable<Animal> runners = from pet in pets
                                          where pet is IRunner
                                          select pet;

            foreach (IRunner runner in runners)
                runner.Run();
        }

        static void ListClimbers()
        {
            Utilities.Print("\nListing all animals capable of climbing: ", ConsoleColor.Yellow);

            IEnumerable<Animal> climbers = from pet in pets
                                          where pet is IClimber
                                          select pet;
            foreach (IClimber climber in climbers)
                climber.Climb();
        }

        static void ListBiters()
        {
            Utilities.Print("\nListing all animals capable of biting: ", ConsoleColor.Yellow);

            IEnumerable<Animal> biters = from pet in pets
                                          where pet is IBiter
                                          select pet;

            foreach (IBiter biter in biters)
                biter.Bite();
        }
    }
}
