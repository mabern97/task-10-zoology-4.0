﻿using System;
namespace Task10Zoology
{
    public interface IClimber
    {
        public void Climb();
    }
}
