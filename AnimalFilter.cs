﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task10Zoology
{
    public class AnimalFilter
    {
        /*
         * Filters that the filtering system uses
         */
        public enum Filters
        {
            ByAge,
            ByGender,
            ByHeight,
            ByWeight,
            BySkill // Not used in the filter system
        }

        bool filtering; // Is the filtering system active?
        Filters by; // The filter type

        public AnimalFilter()
        {
            by = default;
        }

        // Delegates used by Filter, Filter<T>
        public delegate bool Check(Animal pet);
        public delegate bool Check<T>(Animal pet, T value);

        // Delegate used by RangeFilter
        public delegate bool CheckRange<T>(Animal pet, T min, T max);


        /*
         * Contains all the logic related to the main user input parts of filtering
         * by age, gender, height or weight.
         * 
         * Steps:
         *  1. Ask the user to specify which filter category they wish to use
         *      - If an invalid option is provided, let them either try again
         *        or they have the option of typing 'exit' to exit.
         *  2. Ask the user to specify either input data:
         *      1. First type of input data are minimum and maximum values
         *          - This applies to Filter.ByAge, Filter.ByHeight and Filter.ByWeight
         *      2. The second kind of input is either Male/Female for Filter.ByGender
         *  3. Use LINQ to query for the specified filter
         *  4. Return this as IEnumerable<Animal> for use elsewhere
         */
        public void FilterSearch(ICollection<Animal> pets)
        {
            bool exit = false;
        requestFilter:

            Utilities.Print("\nAvailable Filters: ", ConsoleColor.DarkMagenta);

            string[] filterList = Enum.GetNames(typeof(AnimalFilter.Filters));
            foreach (string filter in filterList.Take(filterList.Count() - 1))
                Utilities.Print($" - {filter}", ConsoleColor.DarkMagenta);

            Utilities.Print("Please specify which filter you wish to use", ConsoleColor.DarkYellow);
            Utilities.Print("Type 'exit' to exit filtering mode.", ConsoleColor.DarkRed);

            Console.Write($"Filter By> ");
            string chosenFilter = Console.ReadLine();

            switch (chosenFilter.ToLower())
            {
                case "byage":
                    filtering = true;
                    by = Filters.ByAge;

                    break;
                case "bygender":
                    filtering = true;
                    by = Filters.ByGender;

                    break;
                case "byheight":
                    filtering = true;
                    by = Filters.ByHeight;
                    break;
                case "byweight":
                    filtering = true;
                    by = Filters.ByWeight;
                    break;
                case "exit":
                    exit = true;
                    break;
                default:
                    Utilities.Print("Invalid filter entered, please try again or type 'exit' to stop filtering", ConsoleColor.DarkRed);
                    if (!exit)
                        goto requestFilter;
                    break;
            }

            if (exit)
                return;

            Utilities.Print($"Filter mode set to '{by}'.", ConsoleColor.DarkCyan);

            // Filter mode operations
            string fInput;

        fMode:
            if (filtering)
            {
                switch(by)
                {
                    case Filters.ByAge:
                        int MinAge = pets.ToList()[0].Age;
                        int MaxAge = pets.ToList()[pets.Count() - 1].Age;

                        IEnumerable<Animal> animalAges = RangeFilter(pets, MinAge, MaxAge, "age", (pet, min, max) =>
                        {
                            return pet.Age > min && pet.Age <= max;
                        });

                        foreach (Animal animal in animalAges)
                            animal.PrintDescription();

                        goto requestFilter;
                    case Filters.ByGender:
                        Utilities.Print("Please enter a gender (Female, Male) you wish to filter by: ");

                        Console.Write($"Filter Mode{(filtering ? $": {by}" : "")}> ");
                        string genderS = Console.ReadLine();

                        Gender gender;

                        switch(genderS.ToLower())
                        {
                            case "female":
                                gender = Gender.Female;
                                break;
                            case "male":
                                gender = Gender.Male;
                                break;
                            default:
                                Utilities.Print("No gender provided, returning to filter menu.", ConsoleColor.DarkRed);
                                goto requestFilter;
                        }

                        IEnumerable<Animal> animalGenders = Filter(pets, by, (pet) =>
                        {
                            return pet.Gender == gender;
                        });

                        Utilities.Print($"Listing all {gender} aniamls: ", ConsoleColor.DarkYellow);

                        foreach (Animal animal in animalGenders)
                            animal.PrintDescription();

                        goto requestFilter;
                    case Filters.ByHeight:
                        List<Animal> heights = (from pet in pets
                                orderby pet.Height ascending
                                select pet).ToList();

                        double MinHeight = heights[0].Height;
                        double MaxHeight = heights[pets.Count() - 1].Height;

                        IEnumerable<Animal> animalHeights = RangeFilter(pets, MinHeight, MaxHeight, "height", (pet, min, max) =>
                        {
                            return pet.Height > min && pet.Height <= max;
                        });

                        foreach (Animal animal in animalHeights)
                            animal.PrintDescription();

                        goto requestFilter;
                    case Filters.ByWeight:
                        List<Animal> weights = (from pet in pets
                                                orderby pet.Weight ascending
                                                select pet).ToList();

                        double MinWeight = weights[0].Weight;
                        double MaxWeight = weights[pets.Count() - 1].Weight;

                        IEnumerable<Animal> animalWeights = RangeFilter(pets, MinWeight, MaxWeight, "weight", (pet, min, max) =>
                        {
                            return pet.Weight > min && pet.Weight <= max;
                        });

                        foreach (Animal animal in animalWeights)
                            animal.PrintDescription();

                        goto requestFilter;
                }

                if (!exit)
                    goto fMode;
            }
        }


        /*
         * RangeFilter (int version) is used to retrieve input for a minimum value and maximum value.
         *
         * Before requesting the user's input, the method requires that a bound is specified
         *      (i.e What is the range of value that can be entered, for the input to be considered
         *      valid.)
         *      
         * Attempt to parse the user's input, then use the below method to provide us with the data
         *      Filter<T>(ICollection<Animal> pets, Filters filter, Check<T> filterCheck, T value)
         */
        public IEnumerable<Animal> RangeFilter(ICollection<Animal> pets, int minimum, int maximum, string question, CheckRange<int> check)
        {
            int MinRange = minimum;
            int MaxRange = maximum;

            Utilities.Print($"The range for {by} is between {MinRange} and {MaxRange}.", ConsoleColor.Cyan);

            // User Input : Minimum Range
            Utilities.Print($"Please enter a minimum {question} you wish to filter: ");
            Console.Write($"Filter Mode{(filtering ? $": {by}" : "")}> ");
            string rangeA = Console.ReadLine();

            if (Int32.TryParse(rangeA, out int rA))
            {
                if (rA >= MinRange && rA < MaxRange)
                    MinRange = rA;
                else
                    Utilities.Print($"Minimum {question} is out of bounds, using '{MinRange}'");

            }
            else
                Utilities.Print($"Invalid minimum {question} entered, using '{MinRange}'");


            // User Input : Maximum Range
            Utilities.Print($"Please enter a maximum {question} you wish to filter: ");
            Console.Write($"Filter Mode{(filtering ? $": {by}" : "")}> ");
            string rangeB = Console.ReadLine();

            if (Int32.TryParse(rangeB, out int rB))
            {
                if (rB >= MinRange && rB <= MaxRange)
                    MaxRange = rB;
                else
                    Utilities.Print($"Maximum {question} is out of bounds, using '{MaxRange}'");

            }
            else
                Utilities.Print($"Invalid minimum {question} entered, using '{MaxRange}'");

            // Retrieve Filtered Data

            Utilities.Print($"Listing all animals between the {question} of {MinRange} and {MaxRange}", ConsoleColor.DarkYellow);
            IEnumerable<Animal> filteredResults = Filter<int>(pets, by, (pet, min, max) => {
                return check(pet, min, max) == true;
            }, MinRange, MaxRange);

            return filteredResults;
        }

        /*
         * RangeFilter (double version) is used to retrieve input for a minimum value and maximum value.
         *
         * Before requesting the user's input, the method requires that a bound is specified
         *      (i.e What is the range of value that can be entered, for the input to be considered
         *      valid.)
         *      
         * Attempt to parse the user's input, then use the below method to provide us with the data
         *      Filter<T>(ICollection<Animal> pets, Filters filter, Check<T> filterCheck, T value)
         */
        public IEnumerable<Animal> RangeFilter(ICollection<Animal> pets, double minimum, double maximum, string question, CheckRange<double> check)
        {
            double MinRange = minimum;
            double MaxRange = maximum;

            Utilities.Print($"The range for {by} is between {MinRange} and {MaxRange}.", ConsoleColor.Cyan);

            // User Input : Minimum Range
            Utilities.Print($"Please enter a minimum {question} you wish to filter: ");
            Console.Write($"Filter Mode{(filtering ? $": {by}" : "")}> ");
            string rangeA = Console.ReadLine();

            if (Double.TryParse(rangeA, out double rA))
            {
                if (rA >= MinRange && rA < MaxRange)
                    MinRange = rA;
                else
                    Utilities.Print($"Minimum {question} is out of bounds, using '{MinRange}'");

            }
            else
                Utilities.Print($"Invalid minimum {question} entered, using '{MinRange}'");


            // User Input : Maximum Range
            Utilities.Print($"Please enter a maximum {question} you wish to filter: ");
            Console.Write($"Filter Mode{(filtering ? $": {by}" : "")}> ");
            string rangeB = Console.ReadLine();

            if (Double.TryParse(rangeB, out double rB))
            {
                if (rB >= MinRange && rB <= MaxRange)
                    MaxRange = rB;
                else
                    Utilities.Print($"Maximum {question} is out of bounds, using '{MaxRange}'");

            }
            else
                Utilities.Print($"Invalid minimum {question} entered, using '{MaxRange}'");

            // Retrieve Filtered Data
            Utilities.Print($"Listing all animals between the {question} of {MinRange} and {MaxRange}", ConsoleColor.DarkYellow);
            IEnumerable<Animal> filteredResults = Filter(pets, by, (pet, min, max) => {
                return check(pet, min, max) == true;
            }, MinRange, MaxRange);

            return filteredResults;
        }

        /*
         * Filter method that uses a Check delegate (returns Boolean value) and
         * filters data based on if the Check returns true for each if-statement
         * passes the Check
         */
        public IEnumerable<Animal> Filter(ICollection<Animal> pets, Filters filter, Check filterCheck)
        {
            IEnumerable<Animal> data = null;

            switch (filter)
            {
                case Filters.ByAge:
                    data = from pet in pets
                           where filterCheck(pet) == true
                           orderby pet.Age ascending
                           select pet;
                    break;
                case Filters.ByGender:
                    data = from pet in pets
                           where filterCheck(pet) == true
                           orderby pet.Gender ascending
                           select pet;
                    break;
                case Filters.ByHeight:
                    data = from pet in pets
                           where filterCheck(pet) == true
                           orderby pet.Height ascending
                           select pet;
                    break;
                case Filters.ByWeight:
                    data = from pet in pets
                           where filterCheck(pet) == true
                           orderby pet.Weight ascending
                           select pet;
                    break;
                case Filters.BySkill:
                    data = from pet in pets
                           where filterCheck(pet) == true
                           orderby pet.Skills ascending
                           select pet;
                    break;
            }

            return data;
        }

        /*
         * Filter method that uses a Check delegate (passes an animal and value)
         * Usage: Uses LINQ to filter animal class properties if the check returns
         *        true.
         */
        public IEnumerable<Animal> Filter<T>(ICollection<Animal> pets, Filters filter, Check<T> filterCheck, T value)
        {
            IEnumerable<Animal> data = null;

            switch (filter)
            {
                case Filters.ByAge:
                    data = from pet in pets
                           where filterCheck(pet, value) == true
                           orderby pet.Age ascending
                           select pet;
                    break;
                case Filters.ByGender:
                    data = from pet in pets
                           where filterCheck(pet, value) == true
                           orderby pet.Gender ascending
                           select pet;
                    break;
                case Filters.ByHeight:
                    data = from pet in pets
                           where filterCheck(pet, value) == true
                           orderby pet.Height ascending
                           select pet;
                    break;
                case Filters.ByWeight:
                    data = from pet in pets
                           where filterCheck(pet, value) == true
                           orderby pet.Weight ascending
                           select pet;
                    break;
                case Filters.BySkill:
                    data = from pet in pets
                           where filterCheck(pet, value) == true
                           orderby pet.Skills ascending
                           select pet;
                    break;
            }

            return data;
        }

        /*
         * Filter method that uses a CheckRange delegate (passes an animal and low/high bounds)
         * Usage: Uses LINQ to filter animal class properties if the check returns
         *        true.
         */
        public IEnumerable<Animal> Filter<T>(ICollection<Animal> pets, Filters filter, CheckRange<T> filterCheck, T minVal, T maxVal)
        {
            IEnumerable<Animal> data = null;

            switch (filter)
            {
                case Filters.ByAge:
                    data = from pet in pets
                           where filterCheck(pet, minVal, maxVal) == true
                           orderby pet.Age ascending
                           select pet;
                    break;
                case Filters.ByGender:
                    data = from pet in pets
                           where filterCheck(pet, minVal, maxVal) == true
                           orderby pet.Gender ascending
                           select pet;
                    break;
                case Filters.ByHeight:
                    data = from pet in pets
                           where filterCheck(pet, minVal, maxVal) == true
                           orderby pet.Height ascending
                           select pet;
                    break;
                case Filters.ByWeight:
                    data = from pet in pets
                           where filterCheck(pet, minVal, maxVal) == true
                           orderby pet.Weight ascending
                           select pet;
                    break;
                case Filters.BySkill:
                    data = from pet in pets
                           where filterCheck(pet, minVal, maxVal) == true
                           orderby pet.Skills ascending
                           select pet;
                    break;
            }

            return data;
        }
    }
}
