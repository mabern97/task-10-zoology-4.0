﻿using System;
namespace Task10Zoology
{
    public class Cat : Animal
    {
        public bool Striped { get; set; }

        public Cat(string name, int age, double weight, double height, Gender gender, string breed = "Domesticated House Cat")
            :base(name, "Cat", breed, age, weight, height, gender)
        {
            Striped = false;
        }

        public override void PrintDescription()
        {
            Utilities.Print($"{Name} is a {Age} year old {Gender.ToString().ToLower()} {(Striped == true ? "striped " : "")}{Species} (Breed: {Breed}). Rawr! (Weight: {Weight}, Height: {Height})");
        }

        public override void Kill(Animal animal)
        {
            Utilities.Print($"{Name} pounced towards a {animal.Species} named {animal.Name}, dinner has been claimed!");
        }

        public Cat Crossbreed(Cat cat, string name)
        {
            string breed = $"{Species[0]}{cat.Species.Substring(1)}";

            Gender gender = this.Gender == Gender.Female && cat.Gender == Gender.Male ? Gender.Male : Gender.Female;

            double averageWeight = (Weight + cat.Weight) / 2;
            double averageHeight = (Height + cat.Height) / 2;

            Cat baby = new Cat(name, 1, averageWeight, averageHeight, gender);
            baby.Breed = $"{Gender} {Species} {gender} {cat.Species}";
            baby.Species = $"{Species}/{cat.Species}";

            Utilities.Print($"Created a {gender} {Species} {cat.Species} cross breed ({breed}), their name is '{name}'. How cute?");

            return baby;
        }
    }

    public class Tiger : Cat, IRunner, IClimber, IBiter
    {

        public Tiger(string name, int age, double weight, double height, Gender gender, string breed)
            : base(name, age, weight, height, gender, breed)
        {
            Species = "Tiger";

            Skills = Skills.Runner | Skills.Climber | Skills.Biter;
        }

        public void Run()
        {
            Utilities.Print($"{Species}::Run: I am a named {Name} and I can run at a speed between 49-65 km/h!");
        }

        public void Climb()
        {
            Utilities.Print($"{Species}::Climb: I am a named {Name} and I am running at a speed between 49-65 km/h!");
        }

        public void Bite()
        {
            Utilities.Print($"{Species}::Bite: I am a named {Name} and if I bite you, i'll probably kill you.. Yeah :/");
        }
    }

    public class Lion : Cat, IRunner, IBiter
    {

        public Lion(string name, int age, double weight, double height, Gender gender, string breed)
            : base(name, age, weight, height, gender, breed)
        {
            Species = "Lion";

            Skills = Skills.Runner | Skills.Biter;
        }

        public void Run()
        {
            Utilities.Print($"{Species}::Run: I am an apex predator named {Name} and I can run at a speed between 49-65 km/h!");
        }

        public void Bite()
        {
            Utilities.Print($"{Species}::Bite: I am a named {Name} and once I bite you i'll just kill you, that's how hungry I am #apex");
        }
    }

    public class Jaguar : Cat, IRunner
    {

        public Jaguar(string name, int age, double weight, double height, Gender gender, string breed)
            : base(name, age, weight, height, gender, breed)
        {
            Species = "Jaguar";

            Skills = Skills.Runner;
        }

        public void Run()
        {
            Utilities.Print($"{Species}::Run: I am an apex predator named {Name} and I can run at a speed between 49-65 km/h!");
        }
    }

    public class Liger : Tiger
    {
        public Liger(string name, int age, double weight, double height, Gender gender)
            : base(name, age, weight, height, gender, "Male Lion Female Tiger")
        {
            Species = "Liger";
        }
    }

    public class Tigon : Tiger
    {
        public Tigon(string name, int age, double weight, double height, Gender gender)
            : base(name, age, weight, height, gender, "Male Tiger Female Lion")
        {
            Species = "Tigon";
        }
    }
}
