﻿using System;
namespace Task10Zoology
{
    public class Dog : Animal
    {
        public Dog(string name, string breed, int age, double weight, double height, Gender gender)
            : base(name, "Dog", breed, age, weight, height, gender)
        {

        }

        public void Pounce(Animal animal)
        {
            Utilities.Print($"{Name} pounced towards a {animal.Species} named {animal.Name}, dinner has been claimed!");
        }
    }

    public class Greyhound : Dog, IRunner
    {
        public Greyhound(string name, int age, double weight, double height, Gender gender)
            : base(name, "Greyhound", age, weight, height, gender)
        {
            Skills = Skills.Runner;
        }

        public void Run()
        {
            Utilities.Print($"{Species}::Run: My name is {Name} and I am probably one of fastest runner dogs out there #kindaProud");
        }
    }

    public class GermanShepherd : Dog, IBiter, IRunner, IClimber
    {
        public GermanShepherd(string name, int age, double weight, double height, Gender gender)
            : base(name, "German Shepherd", age, weight, height, gender)
        {
            Skills |= Skills.Runner | Skills.Climber | Skills.Biter;
        }

        public void Run()
        {
            Utilities.Print($"{Species}::Run: My name is {Name} and I can run pretty fast I think?");
        }

        public void Climb()
        {
            Utilities.Print($"{Species}::Climb: My name is {Name} and I can jump fairly easily, probally will land on you though");
        }

        public void Bite()
        {
            Utilities.Print($"{Species}::Bite: My name is {Name} and my bite force is pretty strong but i'll likely not kill you?");
        }
    }
}
