﻿using System;

namespace Task10Zoology
{
    public class Utilities
    {
        /*
         * Print out content to the console window, with a parameter to set color :-)
         */
        public static void Print(string line, ConsoleColor color = ConsoleColor.White)
        {
            SetConsoleColor(color);
            Console.WriteLine(line);
            SetConsoleColor();
        }

        public static void SetConsoleColor(ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
        }
    }
}
