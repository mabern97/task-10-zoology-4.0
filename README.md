Allow the user to enter a minimum of 6 Animals OR select an existing collection of Animals. Then they should be able to enter filter criteria for sub collections of animals.

Requirements:

·         Use LINQ to search for a sub set of Animals and display them

·         Allow for at lease two forms of filtering. Example by age or by weight.

Note: The filtering functionality available is up to you.

Learn C#: LINQ and Lambda Expressions

Weight: Basic